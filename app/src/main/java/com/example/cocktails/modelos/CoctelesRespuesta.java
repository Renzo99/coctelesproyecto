package com.example.cocktails.modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CoctelesRespuesta {
    @SerializedName("drinks")
    @Expose
    private Coctel[] results;

    public Coctel[] getResults(){

        return results;
    }
    public void setResults(Coctel[] results){
        this.results = results;
    }
}
