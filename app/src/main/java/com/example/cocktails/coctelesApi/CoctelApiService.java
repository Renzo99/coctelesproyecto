package com.example.cocktails.coctelesApi;

import com.example.cocktails.modelos.CoctelesRespuesta;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CoctelApiService {
    @GET("search.php?s")
    Call<CoctelesRespuesta> obtenerListaCocteles();
}
