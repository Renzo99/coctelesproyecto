package com.example.cocktails.coctelesApi;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.text.style.AlignmentSpan;
import android.view.LayoutInflater;
import android.view.VerifiedInputEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cocktails.MainActivity;
import com.example.cocktails.R;
import com.example.cocktails.modelos.Coctel;
import com.example.cocktails.ui.fragmentos.Coctel_Detalle;
import com.example.cocktails.ui.fragmentos.MainFragment;


import java.util.List;

public class ListaCococtelAdapter extends RecyclerView.Adapter<ListaCococtelAdapter.ViewHolder> {

    private List<Coctel> cocteles;
    private Context context;
    private View view;

    public ListaCococtelAdapter(List<Coctel> cocteles, Context context) {
        this.cocteles = cocteles;
        this.context = context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.coctel,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombre.setText(cocteles.get(position).getStrDrink());
        Glide.with(context).load(cocteles.get(position).getStrDrinkThumb())
                //.apply(new RequestOptions().override(800,200))
                .into(holder.foto);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coctel c = cocteles.get(position);
                AppCompatActivity activity = (AppCompatActivity)view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new Coctel_Detalle(c)).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cocteles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nombre;
        private ImageView foto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foto = itemView.findViewById(R.id.foto);
            nombre = itemView.findViewById(R.id.nombre);
        }
    }



}
