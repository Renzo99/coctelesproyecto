package com.example.cocktails.ui.fragmentos;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.cocktails.BuildConfig;
import com.example.cocktails.R;
import com.example.cocktails.coctelesApi.CoctelApiService;
import com.example.cocktails.coctelesApi.ListaCococtelAdapter;
import com.example.cocktails.modelos.Coctel;
import com.example.cocktails.modelos.CoctelesRespuesta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private Retrofit retrofit;
    private List<Coctel> cocteles;
    private RecyclerView recyclerView;
    private ListaCococtelAdapter listaCococtelAdapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.refresh){
            obtenerDatos();
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_fragment, container,
                false);

        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.thecocktaildb.com/api/json/v1/1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        obtenerDatos();
        recyclerView = view.findViewById(R.id.recyclerview);
        cocteles = new ArrayList<>();

        return view;
    }

    // ctr + espacio te da opciones
    private void obtenerDatos() {
        CoctelApiService service = retrofit.create(CoctelApiService.class);
        Call<CoctelesRespuesta> coctelesRespuestaCall = service.obtenerListaCocteles();

        coctelesRespuestaCall.enqueue(new Callback<CoctelesRespuesta>() {
            @Override
            public void onResponse(Call<CoctelesRespuesta> call, Response<CoctelesRespuesta> response) {
                if (response.isSuccessful()) {
                    CoctelesRespuesta coctelesRespuesta = response.body();
                    cocteles = new ArrayList<>(Arrays.asList(coctelesRespuesta.getResults()));
                    Cargar(cocteles);

                } else {
                    Log.e(TAG, "onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<CoctelesRespuesta> call, Throwable t) {
                Toast.makeText(getActivity(),"Refres: Error al recibir datos de la api, compruebe su conexion",Toast.LENGTH_LONG).show();
            }

        });
    }
    private void Cargar(List<Coctel> coctelLista){
        listaCococtelAdapter = new ListaCococtelAdapter(coctelLista, getContext());
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),3,GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(listaCococtelAdapter);
    }

}