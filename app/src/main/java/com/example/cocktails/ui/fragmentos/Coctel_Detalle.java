package com.example.cocktails.ui.fragmentos;

import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.cocktails.R;
import com.example.cocktails.databinding.CoctelDetalleFragmentBinding;
import com.example.cocktails.modelos.Coctel;

import java.util.Objects;

public class Coctel_Detalle extends Fragment {

    private View view;
    private CoctelDetalleViewModel mViewModel;
    private CoctelDetalleFragmentBinding coctelDetalle;
    private Coctel coctelD;


    public Coctel_Detalle() {
    }

    public Coctel_Detalle(Coctel coctelD) {
        this.coctelD = coctelD;
    }

    public static Coctel_Detalle newInstance() {
        return new Coctel_Detalle();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        coctelDetalle = CoctelDetalleFragmentBinding.inflate(inflater);

        view = coctelDetalle.getRoot();

        CargarDatos();

        return view;
    }


    @SuppressLint("SetTextI18n")
    private void CargarDatos(){
        coctelDetalle.NombreCoctel.setText(coctelD.getStrDrink());
        coctelDetalle.Ingredientes.setText(coctelD.getStrIngredient1()+"\n"+coctelD.getStrIngredient2()+"\n"+coctelD.getStrIngredient3());
        coctelDetalle.medidas.setText(coctelD.getStrMeasure1()+"\n"+coctelD.getStrMeasure2()+"\n"+coctelD.getStrMeasure3());
        coctelDetalle.instrucciones.setText(coctelD.getStrInstructions());
        Glide.with(requireContext()).load(coctelD.getStrDrinkThumb())
                .into(coctelDetalle.fotoCoctel);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CoctelDetalleViewModel.class);
        // TODO: Use the ViewModel
    }

}